﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using Calc.Models;

namespace Calc
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Напишите в строку выражение из десятичных дробей без пробелов и нажмите клавишу Enter...");
            Console.WriteLine("Например 23,5+98,055");
            try
            {
                string enter = Console.ReadLine();
                if (!DigitsCommasDotsOnly(enter))
                {
                    if (enter.Contains('+') || enter.Contains('-'))
                    {
                        Operation summation = new Summation(enter);
                        summation.Calculation();
                        summation.Print();

                    }
                    if (enter.Contains('*'))
                    {
                        Operation multiplication = new Multiplication(enter);
                        multiplication.Calculation();
                        multiplication.Print();
                    }
                    if (enter.Contains('/'))
                    {
                        Operation division = new Division(enter);
                        division.Calculation();
                        division.Print();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.WriteLine("{0}: {1}","Method", e.TargetSite);
            }
            
            Console.ReadKey();
        }

        static bool DigitsCommasDotsOnly(string s)
        {
            int len = s.Length;
            for (int i = 0; i < len; ++i)
            {
                char c = s[i];
                if (c < '0' || c > '9' || c != ',' || c != '.' || c != '/' || c != '*' || c != '+' || c != '-')
                    return false;
            }
            return true;
        }
    }
}

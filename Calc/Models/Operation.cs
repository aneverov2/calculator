﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Calc.Models
{
    public abstract class Operation
    {
        protected string Enter;
        protected double A;
        protected double B;
        protected double Res;

        public Operation(string enter)
        {
            Enter = enter;
        }

        public abstract void Calculation();
        public abstract void Print();

    }
}

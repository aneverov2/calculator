﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calc.Models
{
    public class Summation : Operation
    {
        readonly char[] _delimeterChars = new[] { '+', '-', '*', '/' };
        private string[] _result;
        private int _length;
        private int _aLengh;
        private int _bLengh;
        private readonly bool _contains;

        public Summation(string enter) : base(enter)
        {
            _contains = enter.Contains('+');
        }

        public override void Calculation()
        {
            string[] oper = Enter.Split(_delimeterChars);
            A = double.Parse(oper[0].Replace('.', ','));
            B = double.Parse(oper[1].Replace('.', ','));
            if (_contains)
            {
                Res = A + B;
            }
            else
            {
                Res = A - B;
            }
            //Подсчет сдвига слагаемых
            _result = Res.ToString().Split(",");
            _length = _result[1].Length;
            _result = A.ToString().Split(",");
            _aLengh = _result[1].Length;
            _result = B.ToString().Split(",");
            _bLengh = _result[1].Length;
            _aLengh -= _length;
            _bLengh -= _length;
        }
        
        public override void Print()
        {
            Console.WriteLine(A.ToString().PadLeft(11 + _aLengh));
            if (_contains)
            {
                Console.WriteLine(" {0, 0}", '+');
            }
            else
            {
                Console.WriteLine(" {0, 0}", '-');
            }
            Console.WriteLine(B.ToString().PadLeft(11 +_bLengh));
            Console.WriteLine("{0, 10}", "-------------");
            Console.WriteLine(" {0, 10}", Res);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calc.Models
{
    public class Multiplication : Operation
    {
        private int _aInt;
        private string _bStr;

        public Multiplication(string enter) : base(enter)
        {
        }

        public override void Calculation()
        {
            string[] oper = Enter.Split('*');
            A = double.Parse(oper[0].Replace('.', ','));
            B = double.Parse(oper[1].Replace('.', ','));
            Res = A * B;

            _aInt = Int32.Parse(A.ToString().Replace(",", ""));
            _bStr = B.ToString().Replace(",", "");
        }

        public override void Print()
        {
            Console.WriteLine(" {0, 10}", A);
            Console.WriteLine(" {0, 1}", '*');
            Console.WriteLine(" {0, 10}", B);
            int count = 11;
            for (int i = _bStr.Length - 1; i >= 0; i--)
            {
                Console.WriteLine(((int)Char.GetNumericValue(_bStr[i]) * _aInt).ToString().PadLeft(count));
                count--;
            }
            Console.WriteLine("{0, 10}", "-------------");
            Console.WriteLine(" {0, 10}", Res);
        }
    }
}

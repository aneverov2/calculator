﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Calc.Models
{
    public class Division : Operation
    {
        private int _result;
        private int _dividend;
        private int _divisor;
        private int _temp;
        private readonly List<int> _listResults;
        private readonly List<int> _listDivivends;
        private int _aLengh;
        private int _bLengh;


        public Division(string enter) : base(enter)
        {
            _listResults = new List<int>();
            _listDivivends = new List<int>();
        }

        public override void Calculation()
        {
            string[] oper = Enter.Split('/');
            A = double.Parse(oper[0].Replace('.', ','));
            B = double.Parse(oper[1].Replace('.', ','));
            Res = A / B;

            //Определение у какого оператора большее количество символов после запятой
            string[] result = A.ToString().Split(","); ;
            _aLengh = result[1].Length;//длина первого
            result = B.ToString().Split(",");
            _bLengh = result[1].Length;//длина второго
            
            double power = 10;
            if (_aLengh - _bLengh > 0)
            {
                power = Math.Pow(power, _aLengh);
            }
            else
            {
                power = Math.Pow(power, _bLengh);
            }

            _dividend = Convert.ToInt32(A * power); 
            _divisor = Convert.ToInt32(B * power);
            _temp = _dividend;
            

            string resInString = Res.ToString(CultureInfo.InvariantCulture).Replace(".", "");
            _result = _dividend;
            
            for (int i = 0; i < resInString.Length; i++)
            {
                int a = (int) Char.GetNumericValue(resInString[i]); //текущее значение ответа,
                if (a > 0)
                {
                    _result = _divisor * a;
                    while (_result > _dividend)
                    {
                        _dividend *= 10;
                    }
                    _dividend -= _result;

                    _listResults.Add(_result);
                    _listDivivends.Add(_dividend);
                }
            }
        }

        public override void Print()
        {
            Console.WriteLine("{0,20} |{1,5}", _temp, _divisor);
            Console.WriteLine("{0,-20} |{1,5}", "_", "---------------------------------------");
            Console.WriteLine("{0,20} |{1,5}", "", Res);
            for (int i = 0; i < _listResults.Count; i++)
            {
                Console.WriteLine((" " + _listResults[i]).PadLeft(20 + i));
                Console.WriteLine((" --------").PadLeft(20 + i));
                Console.WriteLine(("_" + _listDivivends[i]*10).PadLeft(20 + i));
                
            }
        }
    }
}
